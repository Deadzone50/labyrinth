import random
from tkinter import *
from player import Player

class Generator(object):
		
		def __init__(self,lab,text):
				self.height = lab.get_height()
				self.width = lab.get_width()
				self.possible = []
				self.lab = lab
				self.text = text
				
				side = random.randint(1,4) #witch side to start at
				if (side == 1) or (side == 3):  #left or right
						if side == 1:
								x = 0
						else:
								x = self.width -1
						y = random.randint(1,self.height-2)  #no corners
				else:                           #top or bottom
						x = random.randint(1,self.width-2)   #no corners
						if side == 2:
								y = 0
						else:
								y = self.height -1
				self.start = Cordinate(x,y)
				#self.lab.set_pos(x,y,0)              #starting positon
				self.player = Player(x,y,self.lab,side)
				self.lab.set_player(self.player)	#create the player
				
				if side == 1:                       #add the possible positions
						cord = Cordinate(x+1,y)
				elif side == 3:
						cord = Cordinate(x-1,y)
				elif side == 2:
						cord = Cordinate(x,y+1)
				else:
						cord = Cordinate(x,y-1)
				self.possible.append(cord)
				self.Generate()                     #start loop
				
				e = 0
				while e == 0:
						side = random.randint(1,4) #witch side to end at
						if (side == 1) or (side == 3):  #left or right
								if side == 1:
										x = 0
								else:
										x = self.width -1
								y = random.randint(1,self.height-2)  #no corners
						else:                           #top or bottom
								x = random.randint(1,self.width-2)   #no corners
								if side == 2:
										y = 0
								else:
										y = self.height -1
										
						if side == 1:                           #the position next to it
								xx = x+1
								yy = y
						elif side == 2:
								xx = x
								yy = y+1
						elif side == 3:
								xx = x-1
								yy = y
						elif side == 4:
								xx = x
								yy = y-1
						if self.lab.get_pos(xx,yy) == 0:        #if the position next to it is a path
								if (abs(x - self.start.get_x()) >= (self.lab.get_width()/3)) and (abs(y - self.start.get_y()) >= (self.lab.get_height()/3)): #if its not too close to the start
										self.lab.set_pos(x,y,9)              #end positon
										self.player.set_end(x,y)
										e = 1
						else:
								continue

		def Next(self):                         #return a random next possible position or 0 if none exist
				if len(self.possible) != 0:
						pos = random.choice(self.possible)
						self.possible.remove(pos)
						return pos
				else:
						return 0
				
		def Calc_possible(self,pos):    #calculate possible paths
				x = pos.get_x()
				y = pos.get_y()
				#print "possible:",
				if (x != 1):
					if (self.lab.is_possible(x-1,y) == 1):
						self.possible.append(Cordinate(x-1,y))
						#print x-1,y,
					else:
						self.Remove_possible(x-1,y)
						if self.lab.is_possible(x-1,y) == 'ol':
							#print "overpass l",
							self.lab.set_over(x-1,y,'l')
							self.Remove_possible(x-3,y)
							self.Calc_possible(Cordinate(x-1,y))
							self.Calc_possible(Cordinate(x-3,y))
						
				if (x != (self.lab.get_width() - 2)):
					if(self.lab.is_possible(x+1,y) == 1):
						self.possible.append(Cordinate(x+1,y))
						#print x+1,y,
					else:
						self.Remove_possible(x+1,y)
						if self.lab.is_possible(x+1,y) == 'or':
							#print "overpass r",
							self.lab.set_over(x+1,y,'r')
							self.Remove_possible(x+3,y)
							self.Calc_possible(Cordinate(x+1,y))
							self.Calc_possible(Cordinate(x+3,y))
				if (y != 1):
					if(self.lab.is_possible(x,y-1) == 1):
						self.possible.append(Cordinate(x,y-1))
						#print x,y-1,
					else:
						self.Remove_possible(x,y-1)
						if self.lab.is_possible(x,y-1) == 'ou':
							#print "overpass u",
							self.lab.set_over(x,y-1,'u')
							self.Remove_possible(x,y-3)
							self.Calc_possible(Cordinate(x,y-1))
							self.Calc_possible(Cordinate(x,y-3))
				if (y != (self.lab.get_height() - 2)):
					if(self.lab.is_possible(x,y+1) == 1):
						self.possible.append(Cordinate(x,y+1))
						#print x,y+1,
					else:
						self.Remove_possible(x,y+1)
						if self.lab.is_possible(x,y+1) == 'od':
							#print "overpass d",
							self.lab.set_over(x,y+1,'d')
							self.Remove_possible(x,y+3)
							self.Calc_possible(Cordinate(x,y+1))
							self.Calc_possible(Cordinate(x,y+3))
				#print "\n"
				self.possible = list(set(self.possible))
				
		def Remove_possible(self,x,y):
				for i in self.possible:
						if (i.get_x() == x) and (i.get_y() == y):
								self.possible.remove(i)
								return
				
		def Generate(self):
				pos = self.Next()
				while pos != 0:
						self.lab.set_pos(pos.get_x(),pos.get_y(),0)
						#print "Pos:",pos.get_x(), pos.get_y(), 
						self.Calc_possible(pos)
						#self.lab.draw(self.text)
						pos = self.Next()
				self.lab.draw(self.text)
				self.lab.clean()
														
class Cordinate(object):
		def __init__(self,x,y):
				self.x = x
				self.y = y
		def get_x(self):
				return self.x
		def get_y(self):
				return self.y
				
