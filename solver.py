

class Solver(object):
    
    def __init__(self,lab,text):
        self.text = text
        self.lab = lab
        self.solv()
        
        
    def solv(self):
        found = 1
        while found == 1:
            found = 0
            for x in range(1,self.lab.get_width()-1):
                for y in range(1,self.lab.get_height()-1):
                    if(self.lab.array[y][x]!= 1): #if there not a wall
                        if(self.check(x,y) != 0):
                            #print "s:",x,y
                            found = 1
                            self.lab.draw(self.text)
        
                    
    def check(self,x,y):
        return self.lab.is_deadend(x,y)