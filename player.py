


class Player(object):
    
    def __init__(self,x,y,lab,direction):
        self.x = x
        self.y = y
        self.d = direction      #1-4 clockwise starting from left, where it came from
        self.lab = lab
        self.goal = 0           #end reached
        
    def set_end(self,x,y):
        self.ex = x
        self.ey = y
        
    def getx(self):
        return self.x
    def gety(self):
        return self.y
    def getd(self):
        return self.d
        
    def move(self,direction):
        x = self.x
        y = self.y
        d = self.d
        if direction == "up":
            if y != 0:
                if self.lab.get_pos(x,y) == 3:  #underpass
                    if (d == 2)or(d == 4):                  #if it came form above/below
                        if self.lab.get_pos(x,y-1) != 1:    #if there is not a wall
                            self.y -= 1
                            self.d = 2                      #set the direction
                else:
                    if self.lab.get_pos(x,y-1) != 1:    #if there is not a wall
                        self.y -= 1
                        self.d = 2
        elif direction == "down":
            if y != (self.lab.get_height() - 1):
                if self.lab.get_pos(x,y) == 3:  #underpass
                    if (d == 2)or(d == 4):                  #if it came form above/below
                        if self.lab.get_pos(x,y+1) != 1:    #if there is not a wall
                            self.y += 1
                            self.d = 4
                else:
                    if self.lab.get_pos(x,y+1) != 1:    #if there is not a wall
                        self.y += 1
                        self.d = 4
        elif direction == "left":
            if x != 0:
                if self.lab.get_pos(x,y) == 3:  #underpass
                    if (d == 3)or(d == 1):                  #if it came form right/left
                        if self.lab.get_pos(x-1,y) != 1:    #if there is not a wall
                            self.x -= 1
                            self.d = 1
                else:
                    if self.lab.get_pos(x-1,y) != 1:    #if there is not a wall
                        self.x -= 1
                        self.d = 1
        elif direction == "right":
            if x != (self.lab.get_width() -1):
                if self.lab.get_pos(x,y) == 3:  #underpass
                    if (d == 1)or(d == 3):                  #if it came form right/left
                        if self.lab.get_pos(x+1,y) != 1:    #if there is not a wall
                            self.x += 1
                            self.d = 3
                else:
                    if self.lab.get_pos(x+1,y) != 1:    #if there is not a wall
                        self.x += 1
                        self.d = 3
        if (self.ex == self.x)and(self.ey == self.y):   #end reached
            self.goal = 1