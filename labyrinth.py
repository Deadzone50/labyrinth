from tkinter import *
class Labyrinth(object):
		def __init__(self,columns,rows):
				'''
				Initiates a new empty labyrinth
				'''
				self.c = columns
				self.r = rows
				self.x1 = 0
				self.x2 = 0
				self.y1 = 0
				self.y2 = 0
				self.array = [ [ 1 for i in range(columns) ] for j in range(rows) ]
				
		def move_view(self,x,y):
			if (0 <= (self.x1 + x))and((self.x2 + x) <= (self.c -1)): #if the viewing area is inside the labyrint
				self.x1 += x
				self.x2 += x
			if (0 <= (self.y1 + y))and((self.y2 + y) <= (self.r - 1)): #if the viewing area is inside the labyrint
				self.y1 += y
				self.y2 += y
		
		def set_view(self,x,y):
			self.x2 = x -1
			self.y2 = y -1
				
		def set_player(self,player):
			self.player = player	
			
		def draw(self,text): #define viewing area
				px = self.player.getx()	#get x and y for the player
				py = self.player.gety()
				text.delete(1.0,END)
				x1 = self.x1
				x2 = self.x2
				y1 = self.y1
				y2 = self.y2
				for i in range(y1,y2+1):
					tmp = ''
					for j in range(x1,x2+1):
						if (j == px) and (i == py):
							tmp += '@'					#player	
						else:
							if self.array[i][j] == 0:   #path
								tmp += '+'
							elif self.array[i][j] == 1: #wall
								tmp += '#'
							elif self.array[i][j] == 2: #overpass
								tmp += '='
							elif self.array[i][j] == 3: #underpass
								tmp += 'X'
							elif self.array[i][j] == 5: #tmp
								tmp += '5'
							elif self.array[i][j] == 9:	#end
								tmp += '+'
										
					text.insert(INSERT, tmp)
					text.insert(INSERT, "\n")
					#print tmp,
					#print '\n',
		
		def save(self,f):
			for i in range(0,self.r):
				tmp = ''
				for j in range(0,self.c):
					if self.array[i][j] == 0:   #path
						tmp += '+'
					elif self.array[i][j] == 1: #wall
						tmp += '#'
					elif self.array[i][j] == 2: #overpass
						tmp += '='
					elif self.array[i][j] == 3: #underpass
						tmp += 'X'
					elif self.array[i][j] == 9: #end
						tmp += '9'
				f.write(tmp+'\n')
		def load(self,f):
			for i in range(0,self.r):
				for j in range(0,self.c):
					tmp = f.read(1)
					if tmp == '\n':
						tmp = f.read(1)
					if tmp == '+':   #path
						self.array[i][j] = 0
					elif tmp == '#':   #wall
						self.array[i][j] = 1
					elif tmp == '=': #overpass
						self.array[i][j] = 2
					elif tmp == 'X': #underpass
						self.array[i][j] = 3
					elif tmp == '9': #end
						self.array[i][j] = 9
						self.player.set_end(j,i)
					else:
						print ("corrupted file")
		def get_height(self):
				return self.r
		
		def get_width(self):
				return self.c
		
		def set_pos(self,x,y,value):
				self.array[y][x] = value
				
		def set_over(self,x,y,t):
				self.set_pos(x,y,2)
				if t == 'l':
					self.set_pos(x-2,y,2)
					self.set_pos(x-1,y,3)
					if self.get_pos(x-1, y+1) == 1:
						self.set_pos(x-1, y+1, 5) # temporary value to make the labyrinth better
					if self.get_pos(x-1, y-1) == 1:
						self.set_pos(x-1, y-1, 5)
				if t == 'r':
					self.set_pos(x+2,y,2)
					self.set_pos(x+1,y,3)
					if self.get_pos(x+1, y+1) == 1:
						self.set_pos(x+1, y+1, 5) # temporary value to make the labyrinth better
					if self.get_pos(x+1, y-1) == 1:
						self.set_pos(x+1, y-1, 5)
				if t == 'u':
					self.set_pos(x,y-2,2)
					self.set_pos(x,y-1,3)
					if self.get_pos(x-1, y-1) == 1:
						self.set_pos(x-1, y-1, 5) # temporary value to make the labyrinth better
					if self.get_pos(x+1, y-1) == 1:
						self.set_pos(x+1, y-1, 5)
				if t == 'd':
					self.set_pos(x,y+2,2)
					self.set_pos(x,y+1,3)
					if self.get_pos(x-1, y+1) == 1:
						self.set_pos(x-1, y+1, 5) # temporary value to make the labyrinth better
					if self.get_pos(x+1, y+1) == 1:
						self.set_pos(x+1, y+1, 5)
				
		def get_pos(self,x,y):
				return self.array[y][x]
		
		def is_possible(self,x,y,o=0): #o = overpass calc
				n = 0
				l = 0
				r = 0
				u = 0
				d = 0
				if (self.get_pos(x, y) != 1):	 #if it is not a wall
						return 0
				if (self.get_pos(x-1, y) != 1):   #check all direction if there is not a wall
						n += 1
						if self.get_pos(x-1, y) == 0:
							l = 1
				if (self.get_pos(x+1, y) != 1):
						n += 1
						if self.get_pos(x+1, y) == 0:
							r = 1
				if (self.get_pos(x, y+1) != 1):
						n += 1
						if self.get_pos(x, y+1) == 0:
							d = 1
				if (self.get_pos(x, y-1) != 1):
						n += 1
						if self.get_pos(x, y-1) == 0:
							u = 1
							
				if (o == 0)and(n == 2):		#overpass, do not calc if already in overpass calculation will end in loop
					if (l == 1) and (r == 1):
						if (x > 3) and (self.is_possible(x-2,y,1)== 1):
							return 'ol'	#overpass left
						if (x+3 < self.get_width()) and (self.is_possible(x+2,y,1)==1):
							return 'or'	#overpass right
					elif (u == 1) and (d == 1):
						if (y > 3) and (self.is_possible(x,y-2,1)== 1):
							return 'ou'	#overpass up
						if (y+3 < self.get_height()) and (self.is_possible(x,y+2,1)==1):
							return 'od'	#overpass down
				if n > 1:
						return 0	#not possible
				return 1	#possible

		def is_deadend(self,x,y,found = 0):
			if (x == self.player.getx())and(y == self.player.gety()):	#if it encounters the player
				return found
			l = 0
			r = 0
			u = 0
			d = 0
			if (self.get_pos(x-1, y) != 1):   #check all direction if there is not a wall
				l = 1
			if (self.get_pos(x+1, y) != 1):
				r = 1
			if (self.get_pos(x, y+1) != 1):
				d = 1
			if (self.get_pos(x, y-1) != 1):
				u = 1
			if self.get_pos(x,y) == 3:				#underpass
				if (l+r+u+d == 1):
					self.set_pos(x,y,0)			#make it a regular path
				if (l+r+u+d == 3):
					self.set_pos(x,y,0)		#make regular
					if l != 1:
						self.set_pos(x,y-1,0)
						self.set_pos(x,y+1,0)
						self.set_pos(x+1,y,1)
						found = self.is_deadend(x+2,y,found +1)
					if r != 1:
						self.set_pos(x,y-1,0)
						self.set_pos(x,y+1,0)
						self.set_pos(x-1,y,1)
						found = self.is_deadend(x-2,y,found +1)
					if u != 1:
						self.set_pos(x-1,y,0)
						self.set_pos(x+1,y,0)
						self.set_pos(x,y+1,1)
						found = self.is_deadend(x,y+2,found +1)
					if d != 1:
						self.set_pos(x-1,y,0)
						self.set_pos(x+1,y,0)
						self.set_pos(x,y-1,1)
						found = self.is_deadend(x,y-2,found +1)
			if self.get_pos(x,y) == 2:				#overpass
				if (l+r+u+d == 1):
					self.set_pos(x,y,1)				#block
					if l == 1:
						self.set_pos(x-1,y,0)
						self.set_pos(x-2,y,1)
						found = 1
					if r == 1:
						self.set_pos(x+1,y,0)
						self.set_pos(x+2,y,1)
						found = 1
					if u == 1:
						self.set_pos(x,y-1,0)
						self.set_pos(x,y-2,1)
						found = 1
					if d == 1:
						self.set_pos(x,y+1,0)
						self.set_pos(x,y+2,1)
						found = 1
			if self.get_pos(x,y) == 0:				#regular path
				if (l+r+u+d == 1):
					self.set_pos(x, y, 1)		#block it
					if l == 1:
						found = self.is_deadend(x-1,y,found +1)	#move to the next
					if r == 1:
						found = self.is_deadend(x+1,y,found +1)
					if u == 1:
						found = self.is_deadend(x,y-1,found +1)
					if d == 1:
						found = self.is_deadend(x,y+1,found +1)

			return found	#return how many blocks it filled
		def clean(self):	#only used to remove temp blocks
			for i in range(0,self.r):
				for j in range(0,self.c):
					if self.get_pos(j,i) == 5:
						self.set_pos(j, i, 1)
