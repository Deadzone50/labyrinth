from generator import Generator
from player import Player
from solver import Solver
from labyrinth import Labyrinth
from tkinter import *
#from tkFont import *
from tkinter.font import *
#from tkFileDialog import *
from tkinter.filedialog import *
#from tkSimpleDialog import *
from tkinter.simpledialog import *
from time import *
class App:
		def __init__(self,master):
			self.frame = Frame(master)
			self.bindkeys()
			self.frame.pack()
			self.buttons(self.frame)
			self.txtframe()
			self.ended = 0
			self.ask = 0
			
		def bindkeys(self):
			self.frame.bind("<Up>",self.keyu)
			self.frame.bind("<Left>",self.keyl)
			self.frame.bind("<Right>",self.keyr)
			self.frame.bind("<Down>",self.keyd)
			
			self.frame.bind("<Shift-Up>",self.keysu)
			self.frame.bind("<Shift-Left>",self.keysl)
			self.frame.bind("<Shift-Right>",self.keysr)
			self.frame.bind("<Shift-Down>",self.keysd)
			
			self.frame.bind("<FocusOut>",self.focus)	#keep the focus and buttons working
		
		def focus(self,event):
			if self.ask == 0:
				self.frame.focus_set()
		
		def keyu(self,event):			#moving the player
			if self.ended == 0:
				self.lab.player.move("up")
				self.lab.draw(self.text)
				if self.lab.player.goal == 1:
					self.end()
		def keyd(self,event):
			if self.ended == 0:
				self.lab.player.move("down")
				self.lab.draw(self.text)
				if self.lab.player.goal == 1:
					self.end()
		def keyl(self,event):
			if self.ended == 0:
				self.lab.player.move("left")
				self.lab.draw(self.text)
				if self.lab.player.goal == 1:
					self.end()
		def keyr(self,event):
			if self.ended == 0:
				self.lab.player.move("right")
				self.lab.draw(self.text)
				if self.lab.player.goal == 1:
					self.end()
			
		def keysu(self,event):
			if self.ended == 0:
				self.lab.move_view(0,-1)
				self.lab.draw(self.text)
		def keysd(self,event):
			if self.ended == 0:
				self.lab.move_view(0,1)
				self.lab.draw(self.text)
		def keysl(self,event):
			if self.ended == 0:
				self.lab.move_view(-1,0)
				self.lab.draw(self.text)
		def keysr(self,event):
			if self.ended == 0:
				self.lab.move_view(1,0)
				self.lab.draw(self.text)
				
		def end(self):
			self.time2 = time() - self.time1
			self.ended = 1
			self.clear()
			self.text.insert(INSERT,"Congratulations you reached the end, your time was: ")
			self.text.insert(INSERT,round(self.time2,2))
			
		def buttons(self,frame):
			Button(frame, text ="Quit",command = frame.quit).pack(side=RIGHT)
			Button(frame, text ="Solve",command = self.solve).pack(side=RIGHT)
			Button(frame, text ="Reset",command = self.reset).pack(side=RIGHT)
			Button(frame, text ="Load",command = self.load).pack(side=RIGHT)
			Button(frame, text ="Save",command = self.save).pack(side=RIGHT)
			#Button(frame, text ="Clear",command = self.clear).pack(side=RIGHT)
			Button(frame,text="New",command = self.new).pack(side=RIGHT)
			
		def txtframe(self):
			self.text = Text(height=40,width=90,font=Font(family="courier new",size = 12))
			self.text.mark_set("tmp", INSERT)
			self.text.mark_gravity("tmp",LEFT)
			self.text.pack()
			
		def new(self):
			self.ask = 1		# gives the focus to the new window
			tmp = askstring("Size", "input size(x y):",initialvalue="90 40")
			self.ask = 0
			if tmp:
				self.ended = 0
				x,y = tmp.split()
				self.text.insert(INSERT,"\nGENERATING")
				print ("\nGENERATING")
				x = int(x)
				y = int(y)
				self.lab=Labyrinth(x,y)
				Generator(self.lab,self.text) #save a temporary file
				f = open('tmp.txt',mode='w')
				self.write(f)
				if x >= 90:
					x = 90
				if y >= 40:
					y = 40
				self.lab.set_view(x,y)	#size of the screen
				
				self.lab.draw(self.text)
				self.time1 = time()
			else:
				print ("no size selected")
			self.frame.focus_set()
			
		def load(self):
			f = askopenfile('r')
			if f:
				self.ended = 0
				self.read(f)
				f.seek(0)						#goto beginning of file
				t = open('tmp.txt',mode='w')	#replace the tmp file
				s = f.read()
				t.write(s)
				self.lab.draw(self.text)
				self.time1 = time()
			else:
				print ("no file selected")
			self.frame.focus_set()
			
		def reset(self):
			f = open('tmp.txt',mode = 'r')
			self.read(f)
			self.lab.draw(self.text)
			self.ended = 0
			self.time1 = time()
			self.frame.focus_set()
			
		def save(self):
			f = asksaveasfile(mode='w')
			if f:
				t = open('tmp.txt',mode='r')	#copy the tmp file
				s = t.read()
				f.write(s)
			else:
				print ("no file selected")
			self.frame.focus_set()
			
		def read(self,f):
			x,y,px,py,d = f.readline().split()		#get the size of the labyrinth, player position and direction
			x = int(x)
			y = int(y)
			px = int(px)
			py = int(py)
			d = int(d)
			self.lab=Labyrinth(x,y)
			
			if x >= 90:
				x = 90
			if y >= 40:
				y = 40
			self.lab.set_view(x,y)	#size of the screen
			
			self.lab.set_player(Player(px,py,self.lab,d))
			self.lab.load(f)
				
		def write(self,f):
			s = str(self.lab.get_width())	#write the size of the labyrinth
			f.write(s+' ')
			s = str(self.lab.get_height())
			f.write(s+' ')
			s = str(self.lab.player.getx())	#write the position of the player
			f.write(s+' ')
			s = str(self.lab.player.gety())
			f.write(s+' ')
			s = str(self.lab.player.getd())
			f.write(s+'\n')
			self.lab.save(f)
			
		def solve(self):
			Solver(self.lab,self.text)
			self.frame.focus_set()
			
		def clear(self):
			self.text.delete(1.0,END)
			self.text.mark_set("tmp", INSERT)
			self.frame.focus_set()
			
if __name__ == "__main__":
	root = Tk()
	app = App(root)
	root.mainloop()

